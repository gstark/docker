AnalysisBase-21.2.X Image Configuration
=======================================

This configuration can be used to build an image providing a completely
standalone installation of an AnalysisBase-21.2.X release.

Building an image for an existing numbered release can be done with:

```bash
docker build -t <username>/analysisbase:21.2.2 --build-arg RELEASE=21.2.2 \
   --compress --squash .
```

Examples
--------

You can find some pre-built images on
[atlas/analysisbase](https://hub.docker.com/r/atlas/analysisbase/).
